'use strict';

var express = require('express');
var fs = require('fs');
var app = express();
var config = {
  'port': process.env.PORT || 1984
};

// Public files
app.use(express.static('public'));

app.get('/', function(req, res) {
  res.sendFile('./public/index.html');
});

app.get('/resume', function(req, res) {
  var resume = './public/resume.pdf';
  fs.readFile(resume, function(err, data) {
    res.contentType('application/pdf');
    res.send(data);
  });
});

var server = app.listen(config.port, function() {
  console.log('...it will never be like ' + config.port);
});
