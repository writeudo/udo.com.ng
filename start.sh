#!/bin/sh

# start NodeJS server and SASS watcher

nodemon server.js &

sass --sourcemap=none --no-cache  --watch public/app/sass/style.scss:public/app/css/style.min.css --style compressed &